import { Component, OnInit } from '@angular/core';
import { Utilisateur } from './Utilisateur';
import { UtilisateurService } from '../service/utilisateur.service';
import { Router } from '@angular/router';
import { Profil } from 'src/app/classe/Profil';
import { ProfilService } from '../service/profil.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  utilisateur = new Utilisateur()
  profilList: Profil[];
  selectedProfil: Profil;
  constructor(private utilisateurService: UtilisateurService,
    private profilService: ProfilService, private router: Router) { }

  ngOnInit() {

    //recuperer le parametre
    let utilisateurId = localStorage.getItem("editUtilisateurId");

    //alert("Invalid action." + utilisateurId);
    console.log(" est dans la boite" + utilisateurId);
    /*if (!utilisateurId) {
      //alert("Invalid action.")
      this.router.navigate(['admin-utilisateur']);
      return;
    }*/

    //alert("Invalid action.")
    this.profilService.getProfils()
      .subscribe(data => {
        //alert("Invalid action.")
        this.profilList = data;
        console.log(this.profilList)
      });

    //faire un selectionner
    //alert(utilisateurId)
    if (utilisateurId != null) {
      this.utilisateurService.getUtilisateurById(+utilisateurId)
        .subscribe(data => {
          this.utilisateur = data[0];
          try {
            this.utilisateur.profil = this.profilService.getProfilById(this.utilisateur.idProfil.valueOf())[0];
          } catch (error) {

          }

        });
    }
  }

  //pour ajouter
  onSubmit() {
    localStorage.removeItem("editUtilisateurId");
    //alert(JSON.stringify(this.utilisateur));
    this.utilisateur.idProfil = this.utilisateur.profil.id;
    if (this.utilisateur != null && this.utilisateur.id == null) {
      this.utilisateurService.createUtilisateur(this.utilisateur)
        .subscribe(data => {
          this.router.navigate(['admin-user']);
        });

    } else {
      this.utilisateurService.updateUtilisateur(this.utilisateur)
        .subscribe(data => {
          this.router.navigate(['admin-user']);
        });
    }
  }

}
