import { Component, OnInit } from '@angular/core';
import { Utilisateur } from './Utilisateur';
import { UtilisateurService } from '../service/utilisateur.service';
import { Router } from '@angular/router';
import { Profil } from 'src/app/classe/Profil';
import { ProfilService } from 'src/app/service/profil.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  utilisateurList: Utilisateur[];
  profilList: Profil[];
  utilisateurId: String;
  titre: String;
  selectedProfil: Profil;
  constructor(private utilisateurService: UtilisateurService, private profilService: ProfilService, private router: Router) {
    localStorage.removeItem("editUtilisateurId");
  }

  ngOnInit() {
    this.utilisateurService.getUtilisateur()
      .subscribe(data => {
        this.utilisateurList = data;
        console.log(this.utilisateurList)
      });
  
  }


  openModal(id: string) {
    this.utilisateurId = id;
  }

  openModalProfil(id: string) {
    this.utilisateurId = id;
    this.profilService.getProfils()
    .subscribe(data => {
      this.profilList = data;
      console.log(this.profilList)
    });
  }

  deleteUtilisateur(utilisateur: Utilisateur): void {
    console.log("suppression de " + utilisateur.id);
    this.utilisateurService.deleteUtilisateur(utilisateur.id)
      .subscribe(data => {
        this.router.navigate(['admin-userr']);
      })
  };

  editUtilisateur(utilisateur: Utilisateur): void {
    localStorage.removeItem("editUtilisateurId");
    localStorage.setItem("editUtilisateurId", utilisateur.id.toString());
    this.router.navigate(['admin-user-ajouter']);
    console.log(localStorage + " " + utilisateur.id);
  };



}