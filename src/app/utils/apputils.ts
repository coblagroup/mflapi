export class AppUtils {
    public static readonly SERVER_PORT = "3000";
    public static readonly CLIENT_PORT = "4200";

    //
    //public static readonly BASE_API_URL = "https://play.dhis2.org/dev/api";
    //public static readonly BASE_API_URL = "http://localhost:3000";
    public static readonly BASE_API_URL = "https://mflapi.herokuapp.com";
    public static readonly BASE_API_URL2 = "http://localhost:8888";
    public static readonly BASE_BACK_URL = "admin-";


    public static readonly LOADING_STATUS = "true fffffffffffffff";
}