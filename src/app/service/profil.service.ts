import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Profil } from '../classe/Profil';
import { AppUtils } from '../utils/apputils';

@Injectable({
  providedIn: 'root',
})
export class ProfilService {
  
  constructor(private http: HttpClient) { }

  getProfils() {
    return this.http.get<Profil[]>(AppUtils.BASE_API_URL+'/profil');
  }

  getProfilById(id: number) {
    try {
      return this.http.get<Profil[]>(AppUtils.BASE_API_URL + '/profil/' + id);
    } catch (error) {
      return null;
    }
  }

  createProfil(profil: Profil) {
    return this.http.post(AppUtils.BASE_API_URL+'/profil', profil);
    //console.log(profil.libelle);
  }

  updateProfil(profil: Profil) {
    return this.http.put(AppUtils.BASE_API_URL + '/profil' , profil);
  }

  deleteProfil(id: Number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/profil/' +id);
  }
}