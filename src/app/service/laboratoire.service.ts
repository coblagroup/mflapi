import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { AppUtils } from '../utils/apputils';
import { Laboratoire } from '../models/Laboratoire';

@Injectable({
  providedIn: 'root',
})
export class PharmarcieService {
  
  constructor(private http: HttpClient) { }

  getLaboratoires() {
    return this.http.get<Laboratoire[]>(AppUtils.BASE_API_URL+'/laboratoire');
  }

  getLaboratoireById(id: number) {
    try {
      return this.http.get<Laboratoire[]>(AppUtils.BASE_API_URL + '/laboratoire/' + id);
    } catch (error) {
      return null;
    }
  }

  createLaboratoire(Laboratoire: Laboratoire) {
    return this.http.post(AppUtils.BASE_API_URL+'/laboratoire', Laboratoire);
    //console.log(Laboratoire.libelle);
  }

  updateLaboratoire(Laboratoire: Laboratoire) {
    return this.http.put(AppUtils.BASE_API_URL + '/laboratoire' , Laboratoire);
  }

  deleteLaboratoire(id: Number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/laboratoire/' +id);
  }
}