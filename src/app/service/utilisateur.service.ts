import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Utilisateur } from '../user/Utilisateur';
import { AppUtils } from '../utils/apputils';

@Injectable({
  providedIn: 'root',
})
export class UtilisateurService {
  
  constructor(private http: HttpClient) { }

  getUtilisateur() {
    return this.http.get<Utilisateur[]>(AppUtils.BASE_API_URL+'/utilisateur');
  }

  getUtilisateurById(id: number) {
    try {
      return this.http.get<Utilisateur[]>(AppUtils.BASE_API_URL + '/utilisateur/' + id);
    } catch (error) {
      return null;
    }
  }

  createUtilisateur(utilisateur: Utilisateur) {
    return this.http.post(AppUtils.BASE_API_URL+'/utilisateur', utilisateur);
    //console.log(Utilisateur.libelle);
  }

  updateUtilisateur(utilisateur: Utilisateur) {
    return this.http.put(AppUtils.BASE_API_URL + '/utilisateur' , utilisateur);
  }

  deleteUtilisateur(id: number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/utilisateur/' +id);
  }
}