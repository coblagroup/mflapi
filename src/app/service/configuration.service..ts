import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { AppUtils } from '../utils/apputils';
import { Configuration } from '../models/Configuration';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {
  
  constructor(private http: HttpClient) { }

  getConfigurations() {
    return this.http.get<Configuration[]>(AppUtils.BASE_API_URL+'/configurations');
  }

  getConfigurationById(id: number) {
    try {
      return this.http.get<Configuration>(AppUtils.BASE_API_URL + '/configurations/' + id);
    } catch (error) {
      return null;
    }
  }

  createConfiguration(configuration: Configuration) {
    //alert(JSON.stringify(configuration));
    return this.http.post(AppUtils.BASE_API_URL+'/configurations', configuration);
    //console.log(configuration.libelle);
  }

  updateConfiguration(configuration: Configuration) {
    return this.http.put(AppUtils.BASE_API_URL + '/configurations' , configuration);
  }

  updateConfigurationWithId(id:Number, configuration: Configuration) {
    //alert(AppUtils.BASE_API_URL + '/configuration/'+id);
    //alert("up "+ JSON.stringify(configuration));
    return this.http.put(AppUtils.BASE_API_URL + '/configurations/'+id , configuration);
  }

  deleteConfiguration(id: Number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/configurations/' +id);
  }
}