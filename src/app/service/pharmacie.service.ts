import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { AppUtils } from '../utils/apputils';
import { Pharmacie } from '../models/Pharmacie';

@Injectable({
  providedIn: 'root',
})
export class PharmarcieService {
  
  constructor(private http: HttpClient) { }

  getPharmacies() {
    return this.http.get<Pharmacie[]>(AppUtils.BASE_API_URL+'/Pharmacie');
  }

  getPharmacieById(id: number) {
    try {
      return this.http.get<Pharmacie[]>(AppUtils.BASE_API_URL + '/Pharmacie/' + id);
    } catch (error) {
      return null;
    }
  }

  createPharmacie(Pharmacie: Pharmacie) {
    return this.http.post(AppUtils.BASE_API_URL+'/Pharmacie', Pharmacie);
    //console.log(Pharmacie.libelle);
  }

  updatePharmacie(Pharmacie: Pharmacie) {
    return this.http.put(AppUtils.BASE_API_URL + '/Pharmacie' , Pharmacie);
  }

  deletePharmacie(id: Number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/Pharmacie/' +id);
  }
}