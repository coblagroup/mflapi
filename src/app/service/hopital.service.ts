import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'
import { AppUtils } from '../utils/apputils';
import { Hopital } from '../models/Hopital';
import { RpReponse } from '../models/RpReponse';

@Injectable({
  providedIn: 'root',
})
export class HopitalService {

  constructor(private http: HttpClient) { }

  getHopitals() {
    return this.http.get<Hopital[]>(AppUtils.BASE_API_URL + '/uniteOrganisations');
  }

  getHopitalById(id: number) {
    try {
      return this.http.get<Hopital[]>(AppUtils.BASE_API_URL + '/uniteOrganisations/' + id);
    } catch (error) {
      return null;
    }
  }

  createHopital(hopital: Hopital) {
    return this.http.post(AppUtils.BASE_API_URL + '/uniteOrganisations', hopital);
    //console.log(Hopital.libelle);
  }

  updateHopital(hopital: Hopital) {
    return this.http.put(AppUtils.BASE_API_URL + '/uniteOrganisations', hopital);
  }

  deleteHopital(id: Number) {
    return this.http.delete(AppUtils.BASE_API_URL + '/uniteOrganisations/' + id);
  }



  rechercherHopital(hopital: Hopital) {
    let params = new HttpParams()
      .set("code", hopital.code.valueOf())
      .set("nom", hopital.nom.valueOf());
    //alert(JSON.stringify(params))
    return this.http.get<Hopital[]>(AppUtils.BASE_API_URL + '/rechercherUnite', { params: params });
  }


  getChargerUnite() {
    return this.http.get<RpReponse>(AppUtils.BASE_API_URL + '/chargerUniteOrganisation');
  }

  getChargerUniteCoordonnee() {
    return this.http.get<RpReponse>(AppUtils.BASE_API_URL + '/chargerUniteCoordonnees');
  }

}