import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Profil } from '../classe/Profil';
import { AppUtils } from '../utils/apputils';
import { RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class StructureService {

  constructor(private http: HttpClient) { }

  getStructures() {
    // let headers : Headers;
    let username = "admin";
    let password = "district";
    //headers.append("Access-Control-Allow-Origin", "*");
    //headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    //headers.append("Access-Control-Allow-Headers", "Origin X-Requested-With, Content-Type, Accept");
    //headers.append("Authorization", `Basic ${window.btoa(username + ':' + password)}`);

    let httpOptions = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin':'*',
        'Authorization':btoa(username + ":" + password)
      })
    };
    let headers = new HttpHeaders();

    headers.append("Authorization", btoa(username + ":" + password));
    headers.append("Access-Control-Allow-Origin", "*");
    headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headers.append("Access-Control-Allow-Headers", "Origin X-Requested-With, Content-Type, Accept");
    /*  return this.http.get<Object[]>(AppUtils.BASE_API_URL
      + '/organisationUnits.json?fields=id,name,level,parent,featureType,coordinates', 
      httpOptions); 
 */
    return this.http.get<Object[]>(AppUtils.BASE_API_URL2
      + '/apididier', 
      httpOptions);
  }

}