import { Component, OnInit } from '@angular/core';
import { StructureService } from 'src/app/service/structure.service';
import { Hopital } from 'src/app/models/Hopital';
import { HopitalService } from 'src/app/service/hopital.service';
import { PublicComponent } from '../public.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-phopital',
  templateUrl: './hopital.component.html',
  styleUrls: ['./hopital.component.css']
})
export class HopitalComponent extends PublicComponent {

  public listHopital = Array<Object>();
  isLoading: boolean;
  isReLoading: boolean;
  public hopital = new Hopital();

  public rechercheForm: FormGroup;
  public codeUnite: FormControl;
  public nomUnite: FormControl;

  constructor(private hopitalService: HopitalService) {
    super();

  }


  ngOnInit() {
    super.ngOnInit();
  }


  createFormControls() {
    this.codeUnite = new FormControl('dddd', Validators.required);
    this.nomUnite = new FormControl('eee', Validators.required);
  }

  createForm() {
    this.rechercheForm = new FormGroup({
      codeUnite: this.codeUnite,
      nomUnite: this.nomUnite,
    });


  }


  rechercher() {
    this.isLoading =true;
    this.isReLoading =false;

    this.hopital.code = this.codeUnite.value;
    this.hopital.nom = this.nomUnite.value;

    this.hopitalService.rechercherHopital(this.hopital)
      .subscribe(data => {
        this.isLoading =false;
        this.isReLoading =true;
        this.listHopital = data;
      });
  }

}
