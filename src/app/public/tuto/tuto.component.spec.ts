import { TestBed, async } from '@angular/core/testing';
import { TutoComponent } from './tuto.component';
describe('TutoComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TutoComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(TutoComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'ngx-loading-app'`, async(() => {
    const fixture = TestBed.createComponent(TutoComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('ngx-loading-app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(TutoComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to ngx-loading-app!');
  }));
});
