import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../_guards';
import { PublicComponent } from './public.component';
import { HopitalComponent } from './phopital/hopital.component';
import { TutoComponent } from './tuto/tuto.component';


const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    // canActivate: [ AuthGuard ],
    children: [
      { path: '', redirectTo: 'hopital', pathMatch: 'full' },
      { path: 'hopital', component: HopitalComponent },
      { path: 'tuto', component: TutoComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
