import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Profil } from './Profil';
import { ProfilService } from 'src/app/service/profil.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  profil = new Profil()

  constructor(private profilService: ProfilService, private router: Router) { }

  ngOnInit() {

    //recuperer le parametre
    let profilId = localStorage.getItem("editProfilId");
    console.log(" est dans la boite" + profilId);
  
    if (profilId!=null) {
      //faire un selectionner
      this.profilService.getProfilById(+profilId)
        .subscribe(data => {
          //alert("Invalid action. list" + profilId)
          this.profil= data[0];
        });
    }
  }

  //pour ajouter
  onSubmit() {
    if (this.profil != null && this.profil.id == null) {
      this.profilService.createProfil(this.profil)
        .subscribe(data => {
          this.router.navigate(['admin-profil']);
        });

    } else {
      this.profilService.updateProfil(this.profil)
        .subscribe(data => {
          this.router.navigate(['admin-profil']);
        });
    }
    localStorage.removeItem("editProfilId");
    localStorage.setItem("feedback", "ok");
    console.log("resul de " + this.profil.libelle);
  }

}
