import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { BackComponent } from '../back.component';
import { ConfigurationService } from 'src/app/service/configuration.service.';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Configuration } from 'src/app/models/Configuration';
import { createTokenForExternalReference } from '@angular/compiler/src/identifiers';
import { Router } from '@angular/router';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { HopitalService } from 'src/app/service/hopital.service';
import { RpReponse } from 'src/app/models/RpReponse';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent extends BackComponent {


  pageTitre = "Configuration";

  public configuration = new Configuration();
  public rpReponse = new RpReponse();

  configurationForm: FormGroup;
  apiUrl: FormControl;
  uniteDefaultQuery: FormControl;
  apiPassword: FormControl;
  apiLogin: FormControl;

  submitted = false;

  constructor(public configurationService: ConfigurationService,
    public router: Router, public hopitalService: HopitalService) {
    super(router);
    this.loading = false;
  }

  
  getForm() {
    return this.configurationForm;
  }



  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.apiUrl = new FormControl('', Validators.required);
    this.uniteDefaultQuery = new FormControl('', Validators.required);
    this.apiLogin = new FormControl('', Validators.required);
    this.apiPassword = new FormControl('', Validators.required);
  }

  createForm() {
    this.configurationForm = new FormGroup({
      apiUrl: this.apiUrl,
      uniteDefaultQuery: this.uniteDefaultQuery,
      apiLogin: this.apiLogin,
      apiPassword: this.apiPassword,
    });

    this.configurationService.getConfigurations().subscribe((data) => {
      if (data != null && data.length > 0) {
        this.configuration = data[0];
        this.configurationForm.patchValue(data[0])
      } else {
      }

    });
  }

  onSubmitForm() {
    //alert(JSON.stringify(this.configuration));
    this.loading = true;
    this.submitted = true;
    this.configuration.apiUrl = this.apiUrl.value;
    this.configuration.apiLogin = this.apiLogin.value;
    this.configuration.apiPassword = this.apiPassword.value;
    this.configuration.uniteDefaultQuery = this.uniteDefaultQuery.value;

    if (this.configuration != null && this.configuration.id != null) {
      this.configurationService.updateConfigurationWithId(this.configuration.id, this.configuration).subscribe(
        //(response) => alert(response),
        (error) => console.log(error)
      );
    } else {
      this.configurationService.createConfiguration(this.configuration).subscribe(
        // (response) => alert(response),
        (error) => console.log(error)
      );
    }
    //this.router.navigate(['/']);
  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  //charger les unites

  chargerUnite() {

    if (this.configuration != null && this.configuration.id != null) {
      this.loading = true;
      this.hopitalService.getChargerUnite().subscribe((data) => {
        if (data != null) {
          this.rpReponse = data
          this.loading = false;
          alert(this.rpReponse.messageReponse);
        } else {
          this.loading = false;
          alert("Aucun données trouvées");
        }

      });
    } else {
      alert("Erreur de connexion avec le serveur");
    }
  }

  chargerUniteCoordonnees() {

    if (this.configuration != null && this.configuration.id != null) {
      this.loading = true;
      this.hopitalService.getChargerUniteCoordonnee().subscribe((data) => {
        if (data != null) {
          this.rpReponse = data
          this.loading = false;
          alert(this.rpReponse.messageReponse);
        } else {
          this.loading = false;
          alert("Aucun données trouvées");
        }

      });
    } else {
      alert("Erreur de connexion avec le serveur");
    }
  }

  isUniteCharger() {
    this.configurationService.getConfigurations().subscribe((data) => {
      if (data != null && data.length > 0) {
        this.configuration = data[0];
        try {
          if (this.configuration.uniteChargee == null || !this.configuration.uniteChargee) {
            return true;
          }
        } catch (error) {

        }
        return false
      } else {
      }

    });

  }

}
