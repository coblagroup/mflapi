import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseListeComponent } from 'src/app/BaseListeComponent';
import { Pharmacie } from 'src/app/models/Pharmacie';
import { PharmarcieService } from 'src/app/service/pharmacie.service';

@Component({
  selector: 'app-pharmacie-list',
  templateUrl: './pharmacie-list.component.html',
  styleUrls: ['./pharmacie-list.component.css']
})
export class PharmacieListComponent extends BaseListeComponent {
  

  pageTitre = "Liste des pharmarcie";

  pharmacieList: Pharmacie[];
  pharmacieId: String;
  titre: String;


  constructor(private pharmacieService: PharmarcieService, private router: Router) {
    super();
  }


  ngOnInit() {
    this.pharmacieService.getPharmacies()
      .subscribe( data => {
          this.pharmacieList = data;
      });
  }

  openModal(id: string) {
    this.pharmacieId = id;
  }

  deletePharmacie(pharmacie: Pharmacie): void {
    console.log("suppression de " + pharmacie.id);
    this.pharmacieService.deletePharmacie(pharmacie.id)
      .subscribe(data => {
        this.router.navigate(['admin-pharmacie']);
      })
  };

  editPharmacie(pharmacie: Pharmacie): void {
    localStorage.removeItem("editPharmacieId");
    localStorage.setItem("editPharmacieId", pharmacie.id.toString());
    this.router.navigate(['pharmacie-ajouter']);
    console.log(localStorage);
  };


}