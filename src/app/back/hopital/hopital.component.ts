import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseListeComponent } from 'src/app/BaseListeComponent';
import { Hopital } from 'src/app/models/Hopital';
import { HopitalService } from 'src/app/service/hopital.service';

@Component({
  selector: 'app-hopital',
  templateUrl: './hopital.component.html',
  styleUrls: ['./hopital.component.css']
})
export class HopitalComponent extends BaseListeComponent {

  pageTitre = "Liste des hopital";

  hopitalList: Hopital[];
  hopitalId: String;
  titre: String;


  constructor(private hopitalService: HopitalService, private router: Router) {
    super();
  }


  ngOnInit() {
    this.hopitalService.getHopitals()
      .subscribe( data => {
          this.hopitalList = data;
      });
  }

  openModal(id: string) {
    this.hopitalId = id;
  }

  deleteHopital(hopital: Hopital): void {
    console.log("suppression de " + hopital.id);
    this.hopitalService.deleteHopital(hopital.id)
      .subscribe(data => {
        this.router.navigate(['admin-hopital']);
      })
  };

  editHopital(hopital: Hopital): void {
    localStorage.removeItem("editHopitalId");
    localStorage.setItem("editHopitalId", hopital.id.toString());
    this.router.navigate(['hopital-ajouter']);
    console.log(localStorage);
  };


}