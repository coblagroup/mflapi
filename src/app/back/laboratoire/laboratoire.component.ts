import { Component, OnInit } from '@angular/core';
import { BaseFormComponent } from 'src/app/BaseFormComponent';

@Component({
  selector: 'app-laboratoire',
  templateUrl: './laboratoire.component.html',
  styleUrls: ['./laboratoire.component.css']
})
export class LaboratoireComponent extends BaseFormComponent {

  pageTitre = "Laboratoire";

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
